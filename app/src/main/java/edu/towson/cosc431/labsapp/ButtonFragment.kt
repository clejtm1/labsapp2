package edu.towson.cosc431.labsapp


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * A simple [Fragment] subclass.
 */
class ButtonFragment : Fragment() {

    // TODO - 5. Create an interface and enum called OnRatingChange

    // TODO - 6. Add a (private) member variable to hold the current rating
    // TODO - 7. Add a private member variable to hold a reference (nullable) to a listener (an implementation of OnRatingChange)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // TODO - 2. Create the view (5 buttons in a horizontal LinearLayout. Background set to the res/drawable/white.png resource)
        // TODO - 4. Inflate your view, set click listeners on all the buttons
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.button_fragment_layout, container, false)
    }

    // TODO - 8. Add a method for setting the rating change listener

    // TODO - 9. Add a method for getting the current rating

    // TODO - 10. Implement onClick and write the logic to set the correct rating

}
