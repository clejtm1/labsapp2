package edu.towson.cosc431.labsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // TODO - 1. Add a <fragment> tag to your main layout

        // TODO - 11. Make your MainActivity implement OnRatingChange

        // TODO - 12. use the supportFragmentManager to find your Fragment and set "this" as the ratingChange listener

        // TODO - 13. (Optional) - use the supportFragmentManager to dynamically add new rating fragments to your main view
    }
}
